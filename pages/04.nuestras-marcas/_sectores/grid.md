---
container:
  attr:
    id: sectores
  class:
    - top-marcas
  is_fluid: false

grid:
    -
        class:
          - justify-content-around
        cols:
            -
              class:
                - col-md-9
              content: |
                ## Sectores
    -
        class:
          - justify-content-center
        cols:
            -
              class:
                - col-md-4
                - col-sm-6
                - usp
                - usp-1
              content: |
                ### Tecnologías de la Información
            -
              class:
                - col-md-4
                - col-sm-6
                - usp
                - usp-2
              content: |
                ### Alimentos y bebidas
            -
              class:
                - col-md-4
                - col-sm-6
                - usp
                - usp-3
              content: |
                ### Real Estate
---
