---
container:
  attr:
    id: marcas
  class:
    - page-section
    - sec-marcas
  is_fluid: true

grid:          
    - class: []
      cols:
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/blackecco-it.png'
              content: |
                Venta de servicios e infraestructura IT para gobierno y corporativos.
                [ver sitio](http://blackeccoit.com?target=_blank&classes=button,button-outline,inverse 'Blackecco IT')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/rentec.png'
              content: |
                Agencia de arrendamiento de equipo y de servicios integrales en tecnología.
                [ver sitio](http://rentec.com.mx?target=_blank&classes=button,button-outline,inverse 'Rentec')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/movil-evolution.png'
              content: |
                Agencia creativa - digital y de marketing experiencial.
                [ver sitio](http://movil-evolution.com.mx?target=_blank&classes=button,button-outline,inverse 'Movil Evolution')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/computianguis.png'
              content: |
                Venta de hardware y submayoreo de tecnología.
                [ver sitio](http://computianguis.com.mx/?target=_blank&classes=button,button-outline,inverse 'Comoputianguis')
    - class:
        - border-top
      cols:
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/last-code.png'
              content: |
                Programación y desarrollo de software.
                [ver sitio](https://last-code.mx/?target=_blank&classes=button,button-outline,inverse 'Last Code')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/social-event.png'
              content: |
                Experiencias tecnológicas para eventos sociales y empresariales.
                [ver sitio](http://socialevent.mx/?target=_blank&classes=button,button-outline,inverse 'Social Event')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/ofertec.png'
              content: |
                Venta de tecnología a través de comercio electrónico.
                [ver sitio](http://www.ofertec.com.mx/?target=_blank&classes=button,button-outline,inverse 'Ofertec')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/salsalab.png'
              content: |
                Agencia de diseño y comunicación visual.
                [ver sitio](http://salsalab.mx/?target=_blank&classes=button,button-outline,inverse 'Salsalab')
    - class:
        - border-top
      cols:
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/la-chinampa.png'
              content: |
                Taqueria 100% mexicana.
                [ver sitio](http://lachinampa.com.mx/desk/?target=_blank&classes=button,button-outline,inverse 'La Chinampa')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/las-ahogadas.png'
              content: |
                Restaurante especializado en la cocina típica de Jalisco.
                [ver sitio](http://lasahogadasdejalisco.com.mx/menu/?target=_blank&classes=button,button-outline,inverse 'Las Ahogadas de Jalisco')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/hancook.png'
              content: |
                Restaurante especializado en hamburguesas gourmet.
                [ver sitio]( https://www.facebook.com/HancookMX/?target=_blank&classes=button,button-outline,inverse 'Hancook')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/los-maneados.png'
              content: |
               Restaurante sonorense de cortes de carne.
                [ver sitio](https://www.facebook.com/LosManeados/?target=_blank&classes=button,button-outline,inverse 'Los Maneados')
    - class:
        - border-top
      cols:
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/blackecco-realestate.png'
              content: |
               Empresa de coworking y manejo de inmuebles.
                [ver sitio](/?target=_blank&classes=button,button-outline,inverse 'Blackecco Real Estate')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/blackecco-pos.png'
              content: |
               Distribución y venta de hardware para puntos de venta (POS).
                [ver sitio](http://blackeccopos.com/?target=_blank&classes=button,button-outline,inverse 'Blackecco POS')
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/marcas/fumed.png'
              content: |
               Fundación enfocada en la generación de emprendedores socialmente responsables.
                [ver sitio](http://fumedac.org/?target=_blank&classes=button,button-outline,inverse 'FUMED')
---
