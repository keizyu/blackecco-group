---
container:
  attr:
    id: nuestras-marcas
  class:
    - top-banner
    - marcas-page-top
  is_fluid: false


grid:
  - class:
      - justify-content-around
    cols:
      - class:
          - col-md-9
        content: |
          # Nuestras Marcas
---
