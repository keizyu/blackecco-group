---
container:
  attr:
    id: acerca-de
  class:
    - page-section
    - sec-acerca-de
  is_fluid: false

grid:
    -
        class:
          - justify-content-center
          - paragraph
        cols:
            -
              class:
                - col-md-6
              content: |
                Como parte del fortalecimiento estratégico de Black Ecco Group, los líderes de cada área buscan de manera constante realizar alianzas comerciales que beneficien a ambas partes.

                A la fecha, estas son algunas de las empresas y marcas con las que se colabora para generar un crecimiento mutuo.

    -
        class:
          - justify-content-center
          - alianzas-logos
        cols:
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/500.jpg'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/cva-logo.png'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/Ssicario.jpg'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/dcm-logo.png'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/ghia-logo.png'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/grupomexico.jpg'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/lenovo-logo.png'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/sagarpa.jpg'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/tianguis.jpg'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/lexmark-logo.png'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/tenda-logo.png'
            -
              class:
                - col-md-2
                - col-sm-6
              template: 'partials/grid/cel-image.twig'
              content:
                media:
                  url: 'theme://images/alianzas/xetux.jpg'
---
