---
container:
  attr:
    id: alianzas-top-banner
  class:
    - top-banner
    - alianzas-page-top
  is_fluid: false


grid:
  - class:
      - justify-content-around
    cols:
      - class:
          - col-md-9
        content: |
          # Alianzas Estratégicas
---
