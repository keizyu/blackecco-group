---
container:
  attr:
    id: slider-alianzas
  class:
    - page-section
    - page-section-alianzas
    - border-bottom
  innercontainer_class: ['carousel']
  is_fluid: false


swiper:
  #as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
  slides:
    -
      name: Lenovo
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: lenovo-logo.png
        content: |
    -
      name: Tenda
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: tenda-logo.png
        content: |
    -
      name: GHIA
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: ghia-logo.png
        content: |
    -
      name: CVA
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: cva-logo.png
        content: |
    -
      name: DCM
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: dcm-logo.png
        content: |
    -
      name: Lexmark
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: lexmark-logo.png
        content: |
---

### Blackecco Group
## Alianzas Estratégicas
