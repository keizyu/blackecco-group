---
container:
  attr:
    id: top-banner
  class:
    - top-banner
    - page-top-quienes-somos
  is_fluid: false


grid:
  - class:
      - justify-content-around
      - wow fadein
    cols:
      - class:
          - col-md-9
        content: |
          # ¿Quiénes Somos?
      - class:
          - col-md-6
        content: |
          **Black Ecco Group** es un nuevo concepto definido como "Communitarian Private Equity", pues mezcla características de un Fondo de Capital Privado, Incubadora de Negocios y Comunidad, para de esta forma generar un ecosistema empresarial innovador.
---
