---
title: "¿Quiénes Somos?"
menu: Home
onpage_menu: false
body:
  attr: {}
  class:
    - sec-quienes-somos
    - modular

metadata:
  robots: 'index, follow'

content:
  items: '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _top
      - _fundadores
      - _black-ban
      - _middle-comunidad
      - _middle-capital
      - _middle-incubadora
      - _alianzas
---
