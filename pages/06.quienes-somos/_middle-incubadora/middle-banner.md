---
container:
  attr:
    id: middle-banner
  class:
    - quienes-somos-incubadora
    - heading-ganar
  is_fluid: false
  
media:
    position: right
---

## incubadora de negocios
Los nuevos socios se benefician de toda la estructura de nuestro corporativo (RH, Legal, Contabilidad, CXC, Finanzas, Tesorería, Logística, etc.) para de esta forma enfocarse en sus objetivos y acelerar el crecimiento de sus unidades de negocio. Brindamos asesoría y coaching de manera constante para hacer más eficiente el trabajo y fomentar habilidades de liderazgo.
