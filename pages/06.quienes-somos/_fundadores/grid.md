---
container:
  attr:
    id: fundadores
  class:
    - page-section
    - sec-founders
  is_fluid: true

grid:          
    - class: []
      cols:
          -
            class:
              - col-md-4
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              founder:
                name: René Quintero R.
                position: Founder/CEO
              media:
                url: 'theme://images/fundadores/rene-quintero.jpg'
              social:
                fb:
                  attr:
                    data-ico: '&#xE00B;'
                  classes:
                    - ico
                    - ico-standalone
                  name: '/facebook'
                  link: 'https://www.facebook.com/'
                tw:
                  attr:
                    data-ico: '&#xE00A;'
                  classes:
                    - ico
                    - ico-standalone
                  name: '@twitter'
                  link: 'https://twitter.com/twitter'
                in:
                  attr:
                    data-ico: '&#xE00D;'
                  classes:
                    - ico
                    - ico-standalone
                  name: 'linkedin'
                  link: 'https://linkedin.com/'
              content: |
                > "Estamos viviendo una revolución digital en la que se generan nuevos paradigmas disruptivos, principalmente en los negocios, no importa el área, la tecnología es el nuevo insumo básico que marca un nuevo ritmo y reglas.
                Es por esto que en  Black Ecco  creemos que nuestra especialización, la tecnología, nos hace innovadores  y con la capacidad de hacer las cosas diferentes en distintos nichos."
          -
            class:
              - col-md-4
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              founder:
                name: James Kramer U.
                position: Founder/CFO
              media:
                url: 'theme://images/fundadores/james-kramer.jpg'
              social:
                fb:
                  attr:
                    data-ico: '&#xE00B;'
                  classes:
                    - ico
                    - ico-standalone
                  name: '/facebook'
                  link: 'https://www.facebook.com/'
                tw:
                  attr:
                    data-ico: '&#xE00A;'
                  classes:
                    - ico
                    - ico-standalone
                  name: '@twitter'
                  link: 'https://twitter.com/twitter'
                in:
                  attr:
                    data-ico: '&#xE00D;'
                  classes:
                    - ico
                    - ico-standalone
                  name: 'linkedin'
                  link: 'https://linkedin.com/'
              content: |
                > “Evidentemente estamos viviendo en la generación de la economía compartida. Hoy más que nunca nos hemos dado cuenta que mientras más compartes, más te toca y ya era hora que esta nueva filosofía llegara a los fondos de inversión.”
          -
            class:
              - col-md-4
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              founder:
                name: Mauricio Martinez V.
                position: Founder/CCO
              media:
                url: 'theme://images/fundadores/mauricio-martinez.jpg'
              social:
                fb:
                  attr:
                    data-ico: '&#xE00B;'
                  classes:
                    - ico
                    - ico-standalone
                  name: '/facebook'
                  link: 'https://www.facebook.com/'
                tw:
                  attr:
                    data-ico: '&#xE00A;'
                  classes:
                    - ico
                    - ico-standalone
                  name: '@twitter'
                  link: 'https://twitter.com/twitter'
                in:
                  attr:
                    data-ico: '&#xE00D;'
                  classes:
                    - ico
                    - ico-standalone
                  name: 'linkedin'
                  link: 'https://linkedin.com/'
              content: |
                > “Todos tenemos aptitudes únicas que fortalecen y forman los pilares de un equipo de trabajo. La curiosidad me llevó a querer descubrir el funcionamiento de cualquier objeto mecánico. Hoy hago lo que más me apasiona, innovación a través de tecnología y creatividad.”

---

### Black Ecco Group
## Nuestros Fundadores
