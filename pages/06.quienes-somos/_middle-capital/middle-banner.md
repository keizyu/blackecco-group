---
container:
  attr:
    id: middle-banner
  class:
    - quienes-somos-capital
    - heading-ganar
  is_fluid: false

media:
    position: left
---

## Fondo de capital privado
Invertimos en startups o negocios innovadores que se encuentren en etapas tempranas de desarrollo con los que se pueda tener una integración vertical u horizontal dentro del ecosistema de Black Ecco Group.

Actualmente participamos en los siguientes sectores:
+ Empresas de Tecnología
+ Empresas de Alimentos y Bebidas
+ Real Estate
