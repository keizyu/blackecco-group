---
container:
  attr:
    id: middle-banner
  class:
    - quienes-somos-comunidad
    - heading-ganar
  is_fluid: false

media:
    position: right
---

## Comunidad
En Black Ecco Group no buscamos empleados, buscamos futuros socios. Creemos en las ventajas de trabajar con personas motivadas y enfocadas para alcanzar sus sueños, por lo que buscamos de forma activa generar una comunidad orientada a ganar-ganar entre sus miembros.
