---
# title: "Contacto"
routable: false

container:
  attr:
    id: contacto
  class:
    - page-section
    - sec-contacto
    - sec-form
  is_fluid: false

row_class: ['contact-container']
col_content_class: ['contact-title col-md-8 offset-md-2']
col_form_class: ['contact-form']

cache_enable: false

form:
    name: contact-form
    classes: 'row'
    fields:
        - name: name
          label: Nombre
          placeholder: Nombre Apellidos
          autofocus: off
          autocomplete: on
          outerclasses: col-md-8 offset-md-2
          type: text
          validate:
            required: true
        - name: empresa
          label: Empresa
          placeholder: Tu empresa
          autofocus: off
          autocomplete: on
          type: text
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: email
          label: Email
          placeholder: correo@dominio.com
          type: email
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: phone
          label: Teléfono
          placeholder: 10 dígitos
          outerclasses: col-md-8 offset-md-2
          type: text
        - name: description
          label: ¿Cómo podemos ayudarte?
          placeholder: Por favor describe cómo podemos ayudarte
          type: textarea
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
    buttons:
        - type: submit
          classes: button-outline inverse
          value: Enviar
    process:
        - save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
        - email:
            to: contacto@blackecco.com
            # to: contacto-2@blackecco.com
            to_name: 'Teresa Soberanes'
            subject: "[Contacto desde el sitio de BlackEcco Group] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - redirect: /gracias
---

Si deseas información o establecer contacto con nosotros, favor de llenar los siguientes campos:
