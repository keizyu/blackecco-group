---
container:
  attr:
    id: google-maps
  class:
    - page-section
    - sec-maps
  is_fluid: false

grid:
    -
        class:
          - direcciones
          - justify-content-center
        cols:
            -
              class:
                - col-md-3
                - col-sm-6
              content: |
                ### Ciudad de México

                Privada Corina 22, Col. del Carmen, 04100 Del. Coyoacán, CDMX

                + <a href="tel:55-5355-3086">(55) 5355 3086</a>
                + <a href="tel:55-2631-7261">(55) 2631 7261</a>
                + <a href="tel:55-2631-7150">(55) 2631 7150</a>
            -
              class:
                - col-md-3
                - col-sm-6
              content: |
                ### Villahermosa, Tabasco

                Calle Oxiacaque 125 Col. Frac Carrizal, Municipio Centro. C.P. 86038, ciudad Villahermosa, Tabasco

                + <a href="tel:993-324-5358">(01 993) 324 5358</a>
            -
              class:
                - col-md-3
                - col-sm-6
              content: |
                ### Estados Unidos

                3204 Crescendo St. Las Vegas Nevada 89129

                + <a href="tel:702-804-0206">(702) 804 0206</a>

---
