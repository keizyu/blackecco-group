---
container:
  attr:
    id: contacto-top-banner
  class:
    - top-banner
    - contacto-page-top
  is_fluid: false


grid:
  - class:
      - justify-content-around
    cols:
      - class:
          - col-md-9
        content: |
          # Contacto
---
