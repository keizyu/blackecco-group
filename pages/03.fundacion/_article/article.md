---
title: 'Fundación del Grupo'
entry:
    bullet: "¿Qué es y cómo surge la Fundación de Black Ecco Group?"
    media:
        thumb: featured-img.png

creator: keizyu
---

La Fundación de Black Ecco es una organización sin fines de lucro, que busca fomentar, asesorar y capacitar a emprendedores socialmente responsables.

Debido a la convicción de los socios fundadores de Black Ecco Group de que la mejor forma de contribuir a la sociedad es generando emprendedores con sentido de responsabilidad, en 2014 nace nuestra fundación como una iniciativa para ayudar a futuros emprendedores.  Como communitarian private equity, Black Ecco Group tiene una firme convicción en la importancia de compartir, ayudar, colaborar, por lo que busca que estos pilares se permeen en los nuevos emprendedores.  

Asimismo, la fundación colabora con el Sistema Educativo Emprendedor (SEDEM) quien busca capacitar e instruir a niños y jóvenes desde temprana edad en los temas de emprendimiento.  Actualmente se tienen acuerdos con instituciones tanto privadas como públicas, donde a partir de una metodología de estudio, los niños y jóvenes aprenden conceptos y habilidades básicas que se necesitan para tener mayor probabilidad de éxito una vez que busquen emprender en el mundo no escolar.

Es importante mencionar que la Fundación Black Ecco Group busca generar modelos de negocios que no sólo busquen el beneficio individual, sino que retribuyan a nuestra sociedad.
