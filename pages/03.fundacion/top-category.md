---
title: "Fundación México Diferente, A.C."
menu: Home
onpage_menu: false
body:
  attr: {}
  class:
    - sec-fumed
    - modular

metadata:
  robots: 'index, follow'

content:
  items: '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _article
      - _contact
---
