---
title: "Home"
menu: Home
onpage_menu: false
body:
  attr: {}
  class:
    - be-home
    - modular

metadata:
  robots: 'index, follow'

content:
  items:
    - '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _top
      - _proyectos
      - _parallax
      - _mision
      - _news
      - _marcas
---
