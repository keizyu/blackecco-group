---
container:
  attr:
    id: slider-marcas
  class:
    - page-section
    - page-section-marcas
    - border-bottom
  innercontainer_class: ['carousel']
  is_fluid: false


swiper:
  #as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
  slides:
    -
      name: Movil Evolution
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _movil-evolution.png
        content: |
    -
      name: La Chinampa
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _la-chinampa.png
        content: |
    -
      name: Computianguis
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _computianguis.png
        content: |
    -
      name: Rentec
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _rentec.png
        content: |
    -
      name: Social Event
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _social-event.png
        content: |
    -
      name: Blackecco IT
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _blackecco-it.png
        content: |
    -
      name: Last Code
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _last-code.png
        content: |
    -
      name: Ofertec
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _ofertec.png
        content: |
    -
      name: Salsalab
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _salsalab.png
        content: |
    -
      name: Las Ahogadas de Jalisco
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _las-ahogadas.png
        content: |
    -
      name: Hancook
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _hancook.png
        content: |
    -
      name: Los Maneados
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _los-maneados.png
        content: |
    -
      name: Blackecco Real Estate
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _blackecco-realestate.png
        content: |
    -
      name: Blackecco POS
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: _blackecco-pos.png
        content: |
---
### equipo de negocios
## nuestras marcas
