---
container:
  attr:
    id: home-banner
  class:
    - home-top-banner
  is_fluid: false

grid:
  - class:
      - justify-content-center
    cols:
      - class:
          - banner-container
          - col-md-12
        children:
          cols:
            - class:
                - col-lg-7
                - banner-heading
              content: |
                # **Communitarian**<br />private equity
            - class:
                - col-lg-4
                - banner-content
              content: |
                **Black Ecco Group** es un<br />"Communitarian Private Equity" que busca potencializar el talento humano a través de la tecnología y las relaciones personales.

  - class: []
    cols:
      - class:
          - col-lg-12
        content: |
          [Descubre más](/quienes-somos?classes=button,button-outline)
          <div class="mouse-icon"><div class="wheel"></div></div>
---
