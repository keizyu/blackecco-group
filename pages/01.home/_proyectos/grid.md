---
container:
  attr:
    id: numeros
  class:
    - page-section
    - sec-numeros
  is_fluid: false

grid:
    -
        class:
          - counter-container
          - wow
          - fadein
        cols:
            -
              class:
                - col-md-3
                - col-sm-6
              content: |
                <div class="counter" size="2" data-count="07">00</div>
                ### TRAYECTORIA EN AÑOS
            -
              class:
                - col-md-3
                - col-sm-6
              content: |
                <div class="counter" data-count="15">00</div>
                ### EMPRESAS
            -
              class:
                - col-md-3
                - col-sm-6
              content: |
                <div class="counter" data-count="432">00</div>
                ### COLABORADORES
            -
              class:
                - col-md-3
                - col-sm-6
              content: |
                <div class="counter" data-count="02">00</div>
                ### PAÍSES
---

### Black Ecco Group en números
## Cientos de proyectos en un sólo lugar
