---
container:
  attr:
    id: parallax-banner
  class:
    - page-parallax-mejor-concepto
    - parallax-banner
  is_fluid: false


---

# Tomamos <br /> <span class="highlight">lo mejor</span> de cada concepto
## _fondo de capital<br />privado_
## _incubadora de<br />negocios_
## _comunidad_

[Descubre más](/nuestras-marcas?classes=button,button-outline)
