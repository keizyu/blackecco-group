---
container:
  attr:
    id: nuestra-historia-content
  class:
    - page-section
    - page-section-nuestra-historia
  is_fluid: false


grid:
    - class: []
      cols:
          -
            class:
              - col-sm-4
              - offset-sm-2
              - text-right
            content: |
              # 2010
              ## Fundación
              Mauricio Martínez Vargas, René Quintero Ramos y James Kramer Ugarte sientan las bases de lo que será conocido como <br /> **Black Ecco Group** tras fusionar sus empresas: **Computianguis SA de CV** y **Movil Evolution SA de CV**; las cuales se encontraban en el giro de TI.
          -
            class:
              - col-sm-4
              - text-left
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/historia/pic-2010.png'
    - class: []
      cols:
          -
            class:
              - col-sm-4
              - offset-sm-2
              - text-right
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/historia/pic-2012.png'
          -
            class:
              - col-sm-4
              - text-left
            content: |
              # 2012
              ## Primer Restaurante
              Se abre el primer restaurante bajo la marca **"Las Ahogadas de Jalisco"**.
    - class: []
      cols:
          -
            class:
              - col-sm-4
              - offset-sm-2
              - text-right
            content: |
              # 2013
              ## Primera Adquisición
              Se adquiere el 50% de la empresa dueña de las marcas **"La Chinampa"** y **"Los Maneados"**. Actualmente esta unidad de negocio opera 9 restaurantes y "La Chinampa" fue nombrada como imagen para la Semana Nacional del Emprendedor 2015.
          -
            class:
              - col-sm-4
              - text-left
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/historia/pic-2013.png'
    - class: []
      cols:
          -
            class:
              - col-sm-4
              - offset-sm-2
              - text-right
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/historia/pic-2014.png'
          -
            class:
              - col-sm-4
              - text-left
            content: |
              # 2014
              ## Regionalización y Fundación
              Derivado de la convicción de los socios fundadores de que a México le faltan emprendedores, en 2014 se adquiere **Fundación México Diferente AC (FUMED)** la cual busca promover el emprendimiento y la asesoría a PYMES, para así impulsar la economía nacional.
    - class: []
      cols:
          -
            class:
              - col-sm-4
              - offset-sm-2
              - text-right
            content: |
              # 2015
              ## Internacionalización
              Se apertura la primer oficina internacional en el estado de Nevada, EEUU.

              Se funda **RENTEC**, empresa dedicada a la renta de tecnología para la producción de eventos.
          -
            class:
              - col-sm-4
              - text-left
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/historia/pic-2015.png'
    - class: []
      cols:
          -
            class:
              - col-sm-4
              - offset-sm-2
              - text-right
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/historia/pic-2016.png'
          -
            class:
              - col-sm-4
              - text-left
            content: |
              # 2016
              ## Consolidación
              La empresa traslada su corporativo a la Colonia Del Carmen, Coyoacán en México DF, donde se tiene la capacidad para albergar a más de 150 colaboradores.

              Se empiezan a adquirir empresas siguiendo una integración horizontal y vertical:
              + Se adquiere el 50% de la empresa de diseño **SalsaLab**.
              + Se adquiere el 60% de la empresa **LastCode** especializada en desarrollo.
    - class: []
      cols:
          -
            class:
              - col-sm-4
              - offset-sm-2
              - text-right
            content: |
              # 2017
              ## Evolución
              Black Ecco se da cuenta que su modelo ha evolucionado y decide redefinirse como un **"Communitarian Private Equity"** pues esta definición describe mucho mejor en lo que se convirtió Black Ecco Group.

              Se funda **Social Event**, empresa enfocada en proveer tecnología de punta a eventos sociales y empresariales, para crear experiencias innovadoras y memorables.

              Se funda **Black Ecco Real Estate**, empresa dedicada a administrar los bienes inmuebles del grupo.
          -
            class:
              - col-sm-4
              - text-left
            template: 'partials/grid/cel-image.twig'
            content:
              media:
                url: 'theme://images/historia/pic-2017.png'
---
