---
container:
  attr:
    id: top-banner
  class:
    - top-banner
    - page-top-nuestra-historia
  is_fluid: false


grid:
  - class:
      - justify-content-around
    cols:
      - class:
          - col-md-9
        content: |
          # Nuestra Historia
---
