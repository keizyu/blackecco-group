---
container:
  attr:
    id: black-banner
  class:
    - page-section
    - page-section-black
  is_fluid: false


grid:
    -
        class:
          - justify-content-center
          - wow
          - fadein
        cols:
            -
              class:
                - col-md-9
              content: |
                ## <span class="highlight">simplemente</span> amamos lo que hacemos

---
