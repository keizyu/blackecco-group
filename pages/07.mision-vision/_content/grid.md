---
container:
  attr:
    id: mision-vision-content
  class:
    - page-section
    - page-section-mision-vision
  is_fluid: false


grid:
    -
        class: []
        cols:
            -
              class:
                - col-md-4
                - offset-md-2
                - heading-ganar
              content: |
                ## creamos continuamente lazos con toda nuestra comunidad en una relación <br /> **ganar - ganar**
            -
              class:
                - col-md-4
              content: |
                ## Misión
                Ser el nuevo referente en la forma en la que se estructuran las organizaciones a nivel mundial. Somos un modelo celular de gestión empresarial que con el mejor talento y un entorno más libre, permite desarrollar ideas y proyectos innovadores.

                ## Visión
                Impactar al mundo, a nuestra comunidad y a nuestros colaboradores, apoyando la construcción y la consolidación de sueños empresariales.

                ## Valores
                Colaboración, esfuerzo, equidad, integridad, respeto, responsabilidad.

---
