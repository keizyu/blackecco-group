---
title: "Misión y Visión"
menu: Home
onpage_menu: false
body:
  attr: {}
  class:
    - sec-mision-vision
    - modular

metadata:
  robots: 'index, follow'

content:
  items: '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _top
      - _content
      - _black-ban
---
