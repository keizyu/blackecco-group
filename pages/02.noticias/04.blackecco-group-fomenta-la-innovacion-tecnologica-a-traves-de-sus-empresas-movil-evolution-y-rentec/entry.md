---
title: 'Black Ecco Group fomenta la innovación tecnológica a través de sus empresa: Movil Evolution y Rentec'
entry:
    id: Y10008004
    balazo: "Durante 2010 y 2015, respectivamente, Black Ecco Group se asoció con Movil Evolution y fundó Rentec, ambas empresas pertenecientes al giro de innovación tecnológica."
    media:
        thumb: featured-img.png

creator: Blackecco
---

Movil Evolution agencia digital y de marketing experiencial, durante años se ha consolidado como una pionera en desarrollo e innovación tecnológica, logrando grandes experiencias en el sector publicitario BTL (below the line) para marcas y agencias. Rentec por su parte, una empresa más joven, se ha convertido en una gran facilitadora de soluciones integrales para magnos eventos.

Gracias a la inversión y apoyo que Black Ecco Group ha brindado a estas empresas, Movil Evolution y Rentec han podido trabajar a la par en reconocidos proyectos como la Fórmula E, el lanzamiento en México de la camioneta Nissan Kicks y el primer show de drones en México.

Con esto queda en claro que Black Ecco Group apuesta por invertir en nuevas tecnologías y startups creativas que brindarán beneficios económicos para el grupo.
