---
title: 'Black Ecco Group acondiciona su HQ para recibir a más colaboradores '
entry:
    id: Y10008003
    balazo: "En los últimos meses el crecimiento de Black Ecco Group ha sido más que considerable. En menos de seis meses, más de 80 colaboradores se  han unido a la comunidad Black Ecco. Como consecuencia de esto, los headquarters del grupo, ubicados en la colonia Del Carmen Coyoacán, al sur de la CDMX se han visto en la necesidad de ser acondicionados para recibir cálidamente a sus futuros colaboradores."
    media:
        thumb: featured-img.png

creator: Blackecco
---

Actualmente el headquarter en Coyoacán, ofrece diversos amenities como coffee break y comedor, y actividades recreativas como liga de futbol soccer, noches de poker y FIFA. Al estar en una de las zonas más tranquilas y de ambiente agradable, la comunidad Black Ecco convive alegremente en un ambiente amigable, donde de manera sencilla se puede caminar para buscar un café o un rico bocadillo.

Con este crecimiento y la adquisición del mejor talento, Black Ecco Group se consolida como  pionero en el sector de TI, alimentos y bebidas y real estate.
