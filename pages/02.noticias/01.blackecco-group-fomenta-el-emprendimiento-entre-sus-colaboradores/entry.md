---
title: 'Black Ecco Group fomenta el emprendimiento entre sus colaboradores'
entry:
    id: Y10008002
    balazo: "Black Ecco Group, el primer “communitarian private equity”, concepto adoptado por el grupo ya que mezcla características de un fondo de capital privado,  incubadora de negocios y comunidad, se ha posicionado como un lugar donde cada uno de sus colaboradores puede convertirse en un emprendedor exitoso."
    media:
        thumb: featured-img.png

creator: keizyu
---

Black Ecco Group busca crear lazos entre toda la comunidad, en una relación ganar - ganar, donde no se busca tener a un empleado más, sino un posible futuro socio.

El grupo día a día, ofrece diversos entrenamientos que buscan mejorar la eficacia, ampliar y actualizar los conocimientos de todos los colaboradores. Además existe la posibilidad que los miembros de la comunidad, que así lo requieran, se capaciten a través de diplomados, cursos o maestría, recibiendo el apoyo de Black Ecco Group.

Por último, pero no menos importante, en el grupo existe la aceptación para escuchar a quienes tengan una visión de emprendimiento. Los miembros que tengan una idea innovadora o un plan de negocios concreto, y en el cual Black Ecco Group pueda involucrarse, recibirán ayuda y asesoría para desarrollar este proyecto.
