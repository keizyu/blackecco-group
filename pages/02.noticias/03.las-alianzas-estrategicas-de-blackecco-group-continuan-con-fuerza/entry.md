---
title: 'Las alianzas estratégicas de Black Ecco Group continúan con fuerza'
entry:
    id: Y10008001
    balazo: "Como parte del fortalecimiento estratégico del grupo, en los últimos meses Black Ecco Group ha realizado dos alianzas estratégicas importantes: Xetux y Lenovo México."
    media:
        thumb: featured-img.png

creator: keizyu
---

Con estos pactos comerciales que beneficiarán a ambas partes, se busca robustecer al grupo, así como eficientar diversos procesos.

Actualmente Xetux brinda soluciones integrales para restaurantes a través de herramientas de alta tecnología, aunado con la tecnología que Black Ecco ofrece en software y hardware para puntos de venta, los negocios podrán beneficiarse en la mejora de su gestión, desde el proceso de adquisición hasta la recepción de mercancía.

Por otro lado, la asociación con Lenovo México, sitúa al grupo como uno de los distribuidores oficiales de la marca china y fabricante de ordenadores, tabletas y smartphones. Con esto señalamos la seriedad que posee Black Ecco Group, al contar con el respaldo y confianza del fabricante internacional de ordenadores líder en ventas más grande del mundo.

Las negociaciones y alianzas por parte del team Black Ecco siguen día a día, para consolidarse como líder en cada uno de las áreas de negocios en las que se involucra.
